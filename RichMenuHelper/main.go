package main

import (
	"flag"
	"fmt"
	"github.com/line/line-bot-sdk-go/linebot"
	"log"
)

func main() {
	var (
		mode     = flag.String("mode", "list", "mode of richmenu helper [list|create|link|unlink|bulklink|bulkunlink|get|delete|upload|download]")
		uid      = flag.String("uid", "", "user id")
		rid      = flag.String("rid", "", "richmenu id")
		filePath = flag.String("image.path", "", "path to image, used in upload/download mode")
	)
	flag.Parse()
	bot, err := linebot.New("431572341033516753b0dbd1f17c7a1e", "7/71SFMRwyMQ0HiU8swNYJBrhHkblJVQjISWE2DTrz6HIgF3Fqdaf2FNjRe3Y+zQtdlO8Zk8HxIx3qBikuQZbAt/UttJRcW3Iu43JOPjKiL+bQZXxAOGZDX+Du/Tt2mBciiGyDLyQZqiRAGTALCa2AdB04t89/1O/w1cDnyilFU=")
	if err != nil {
		log.Fatal(err)
	}

	switch *mode {
	case "upload":
		if _, err = bot.UploadRichMenuImage(*rid, *filePath).Do(); err != nil {
			log.Fatal(err)
		}
	case "link":
		if _, err = bot.LinkUserRichMenu(*uid, *rid).Do(); err != nil {
			log.Fatal(err)
		}
	case "unlink":
		if _, err = bot.UnlinkUserRichMenu(*uid).Do(); err != nil {
			log.Fatal(err)
		}
	case "bulklink":
		if _, err = bot.BulkLinkRichMenu(*rid, *uid).Do(); err != nil {
			log.Fatal(err)
		}
	case "bulkunlink":
		if _, err = bot.BulkUnlinkRichMenu(*uid).Do(); err != nil {
			log.Fatal(err)
		}
	case "list":
		res, err := bot.GetRichMenuList().Do()
		if err != nil {
			log.Fatal(err)
		}
		for _, richmenu := range res {
			fmt.Printf("%+v\n\n\n", richmenu)

		}
	case "get_default":
		res, err := bot.GetDefaultRichMenu().Do()
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("%v\n", res)
	case "set_default":
		if _, err = bot.SetDefaultRichMenu(*rid).Do(); err != nil {
			log.Fatal(err)
		}
	case "cancel_default":
		if _, err = bot.CancelDefaultRichMenu().Do(); err != nil {
			log.Fatal(err)
		}
	case "create":
		/*richMenu := linebot.RichMenu{
			Size:        linebot.RichMenuSize{Width: 2500, Height: 1686},
			Selected:    false,
			Name:        "productList",
			ChatBarText: "快捷功能",
			Areas: []linebot.AreaDetail{
				{
					Bounds: linebot.RichMenuBounds{X:0,Y:0,Width:621,Height:797},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "profile",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:663,Y:0,Width:621,Height:797},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "productList",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:1263,Y:0,Width:621,Height:797},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "fireRecord",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:1859,Y:0,Width:641,Height:797},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "toolRecord",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:0,Y:845,Width:2500,Height:841},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "queue",
					},
				},

			},
		}
		res, err := bot.CreateRichMenu(richMenu).Do()
		if err != nil {
			log.Fatal(err)
		}*/


		/*richMenu2 := linebot.RichMenu{
			Size:        linebot.RichMenuSize{Width: 2500, Height: 1686},
			Selected:    false,
			Name:        "productList",
			ChatBarText: "快捷功能",
			Areas: []linebot.AreaDetail{
				{
					Bounds: linebot.RichMenuBounds{X:0,Y:0,Width:751,Height:797},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "scissor",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:805,Y:0,Width:876,Height:797},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "stone",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:1683,Y:0,Width:817,Height:797},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "paper",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:0,Y:847,Width:1185,Height:839},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "defeat",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:1233,Y:847,Width:1267,Height:839},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "report",
					},
				},

			},
		}
		res1, err1 := bot.CreateRichMenu(richMenu2).Do()
		if err1 != nil {
			log.Fatal(err1)
		}*/

		richMenu2 := linebot.RichMenu{
			Size:        linebot.RichMenuSize{Width: 2500, Height: 1686},
			Selected:    false,
			Name:        "productList",
			ChatBarText: "快捷功能",
			Areas: []linebot.AreaDetail{
				{
					Bounds: linebot.RichMenuBounds{X:0,Y:0,Width:547,Height:823},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "1",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:573,Y:0,Width:608,Height:823},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "2",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:1209,Y:0,Width:644,Height:823},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "3",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:1875,Y:0,Width:625,Height:823},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "4",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:0,Y:851,Width:547,Height:835},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "5",
					},
				},
				{
					Bounds: linebot.RichMenuBounds{X:571,Y:851,Width:1929,Height:835},
					Action: linebot.RichMenuAction{
						Type: linebot.RichMenuActionTypeMessage,
						Text: "break",
					},
				},
			},
		}
		res1, err1 := bot.CreateRichMenu(richMenu2).Do()
		if err1 != nil {
			log.Fatal(err1)
		}
		log.Println(res1.RichMenuID)
	case "delete":
		_, err := bot.DeleteRichMenu(*rid).Do()
		if(err != nil) {
			log.Fatal(err)
		}
		log.Println("rm success!!")
	default:
		log.Fatal("implement me")
	}
}