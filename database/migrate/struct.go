package migrate

import "github.com/jinzhu/gorm"

type PlayerInfo struct {
	gorm.Model
	LineId string
	Belongings_invoice int `gorm:"DEFAULT: 0"`
	Belongings_fight int `gorm:"DEFAULT: 0"`
	HasDouble bool `gorm:"DEFAULT: false"`
	HpMax int `gorm:"DEFAULT: 100"`
	Attack int `gorm:"DEFAULT: 20"`
	Level int `gorm:"DEFAULT: 1"`
	ExValue int `gorm:"DEFAULT: 0"`
	HpBonus int `gorm:"DEFAULT: 0"`
	AttackBonus int `gorm:"DEFAULT: 0"`
	HistoryBuy []HistoryProd `gorm:"ASSOCIATION_FOREIGNKEY:id;FOREIGNKEY:PlayerInfoId"`
	HistoryFight []HistoryFight `gorm:"ASSOCIATION_FOREIGNKEY:id;FOREIGNKEY:PlayerInfoId"`
}

type HistoryProd struct {
	gorm.Model
	Name string
	PlayerInfoId uint
}

type HistoryFight struct {
	gorm.Model
	UidSelf string
	UidOther string
	IsWinner bool
	PlayerInfoId uint
}

func AutoMigrate(db *gorm.DB) {
	db.AutoMigrate(
		&PlayerInfo{},
		&HistoryProd{},
		&HistoryFight{})
}