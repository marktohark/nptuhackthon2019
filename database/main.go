package database

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"nptuhackthon2019/database/migrate"
)

var Db *gorm.DB

func Connect() *gorm.DB  {
	database, err := gorm.Open("mysql", "linebot_nptu:linebot_nptu@tcp(140.130.34.52:33060)/linebot_nptu?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		fmt.Println("database connect fail => ", err)
		return nil
	}
	Db = database
	migrate.AutoMigrate(Db)
	return Db
}