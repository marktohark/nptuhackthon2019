package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/line/line-bot-sdk-go/linebot"
	"net/http"
	"nptuhackthon2019/GameAlgorithm"
	"nptuhackthon2019/StoreObject"
	"nptuhackthon2019/database"
	"nptuhackthon2019/eventHandle"
	"nptuhackthon2019/global"
)

func main() {
	//connect db
	db := database.Connect()
	if db == nil {
		return
	}
	StoreObject.Init()
	GameAlgorithm.Init()
	fmt.Println("begin linebot")
	bot, err := linebot.New("431572341033516753b0dbd1f17c7a1e", "7/71SFMRwyMQ0HiU8swNYJBrhHkblJVQjISWE2DTrz6HIgF3Fqdaf2FNjRe3Y+zQtdlO8Zk8HxIx3qBikuQZbAt/UttJRcW3Iu43JOPjKiL+bQZXxAOGZDX+Du/Tt2mBciiGyDLyQZqiRAGTALCa2AdB04t89/1O/w1cDnyilFU=")
	if err != nil {
		fmt.Println("New err", err)
		return
	}
	global.LineBotApi = bot
	fmt.Println("begin httpfunc")
	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		fmt.Println("client", err)
		w.Write([]byte("test"))
	})
	http.HandleFunc("/callback", func(w http.ResponseWriter, req *http.Request) {
		fmt.Println("line server connect", err)
		events, err := bot.ParseRequest(req)
		if err != nil {
			if err == linebot.ErrInvalidSignature {
				fmt.Println("code:400")
				w.WriteHeader(400)
			} else {
				fmt.Println("code:500")
				w.WriteHeader(500)
			}
			return
		}
		eventHandle.Router(events, bot)
	})
	fmt.Println("begin http server port :3997")
	if err := http.ListenAndServe(":3997", nil); err != nil {
		fmt.Println("fail", err)
		return
	}
}
