package eventHandle

import (
	"github.com/line/line-bot-sdk-go/linebot"
	"nptuhackthon2019/eventHandle/EventType"
)

func Router(events []*linebot.Event, bot *linebot.Client) {
	for _, event := range events {
		switch(event.Type) {
		case linebot.EventTypeMessage:
			EventType.Message(event, bot)
		case linebot.EventTypeBeacon:
			EventType.Beacon(event, bot)
		case linebot.EventTypeFollow:
			EventType.Follow(event, bot)
		case linebot.EventTypeUnfollow:
			EventType.Unfollow(event, bot)
		case linebot.EventTypeJoin:
			EventType.Join(event, bot)
		case linebot.EventTypeLeave:
			EventType.Leave(event, bot)
		case linebot.EventTypePostback:
			EventType.Postback(event, bot)
		}
	}
}