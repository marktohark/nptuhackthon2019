package EventType

import (
	"github.com/line/line-bot-sdk-go/linebot"
	"nptuhackthon2019/CommandHandler"
)

func Message(event *linebot.Event, bot *linebot.Client) {
	switch message := event.Message.(type) {
	case *linebot.TextMessage:
		//if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage(message.Text + "," + event.Source.UserID)).Do(); err != nil {
		//	fmt.Println("replay fail", err)
		//}
		CommandHandler.Parser(message.Text, event, bot)
	}
}