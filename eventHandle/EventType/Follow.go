package EventType

import (
	"fmt"
	"github.com/line/line-bot-sdk-go/linebot"
	"nptuhackthon2019/database"
	"nptuhackthon2019/database/migrate"
	"nptuhackthon2019/global"
	"nptuhackthon2019/struct"
)

func Follow(event *linebot.Event, bot *linebot.Client) {
	uid := event.Source.UserID
	_, ok := global.UserPool[uid]
	if ok {
		if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("你好!!")).Do(); err != nil {
			fmt.Println("replay fail", err)
		}
	} else {
		count := 0
		database.Db.Table("player_infos").Where("line_id = ?", uid).Count(&count)
		if count == 0 {
			database.Db.Create(&migrate.PlayerInfo{
				LineId: uid,
				Belongings_invoice: 0,
				Belongings_fight: 0,
				HasDouble: false,
				HpMax: 100,
				Attack: 20,
				Level: 1,
				ExValue: 0,
			})
		}
		global.UserPool[uid] = &_struct.User{
			Uid: uid,
			State: _struct.Normal,
		}
		if _, err := bot.LinkUserRichMenu(uid, global.RichMenu["mainMenu"]).Do(); err != nil {
			fmt.Println("rich menu fail", err)
		}
		if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("已可以開始使用此服務!!")).Do(); err != nil {
			fmt.Println("replay fail", err)
		}
		fmt.Println("has a client join. uid => ", uid)
	}
}