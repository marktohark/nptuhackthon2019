package global

import (
	"github.com/line/line-bot-sdk-go/linebot"
	"nptuhackthon2019/struct"
)

var (
	UserPool map[string]*_struct.User = make(map[string]*_struct.User)
	RichMenu map[string]string = map[string]string{
		"mainMenu":"richmenu-c848d695a519c949d8c1a442eba5638f",
		"productList":"richmenu-7485a77bb04cbc2c1460c206d0d2f88c",
		"skillList":"richmenu-8970ae611990f3e8f6e20125d0632260",
		"cancelQueue": "richmenu-0f076adce787b3110993defb4e88df7f",
	}
	LineBotApi *linebot.Client
	QueueChannel chan *_struct.User = make(chan *_struct.User, 50)
)