package GameAlgorithm

import (
	"fmt"
	"github.com/line/line-bot-sdk-go/linebot"
	"nptuhackthon2019/global"
	"nptuhackthon2019/struct"
	"time"
)

func QueueThread() {
	for {
		fmt.Println("queue begin")
		time.Sleep(time.Second * 3)
		fmt.Println("begin get channel")
		user1, user2 := <- global.QueueChannel, <- global.QueueChannel
		fmt.Println("get chan ok")
		if user1.State != _struct.Queue || user2.State != _struct.Queue {
			if(user1.State == _struct.Queue) {
				global.QueueChannel <- user1
			}
			if(user2.State == _struct.Queue) {
				global.QueueChannel <- user2
			}
			continue
		}
		if user2.Uid == user1.Uid {
			global.QueueChannel <- user2
			continue
		}
		room := _struct.Room{
			U1: user1,
			U2: user2,
			CreateDate: time.Now(),
		}
		user1.Room = &room
		user2.Room = &room
		user1.State = _struct.Fight_no
		user2.State = _struct.Fight_no
		if _, err := global.LineBotApi.LinkUserRichMenu(user1.Uid, global.RichMenu["skillList"]).Do(); err != nil {
			fmt.Println("link fail => ", err)
			return
		}
		if _, err := global.LineBotApi.LinkUserRichMenu(user2.Uid, global.RichMenu["skillList"]).Do(); err != nil {
			fmt.Println("link fail => ", err)
			return
		}

		if _, err := global.LineBotApi.PushMessage(user1.Uid, linebot.NewTextMessage("開始對戰\n敵方資訊：\n" + Player.InfoStr2(user2))).Do();err != nil {
			fmt.Println("replay fail", err)
			return
		}
		if _, err := global.LineBotApi.PushMessage(user2.Uid, linebot.NewTextMessage("開始對戰\n敵方資訊：\n" + Player.InfoStr2(user1))).Do();err != nil {
			fmt.Println("replay fail", err)
			return
		}
	}
}