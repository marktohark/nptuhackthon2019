package GameAlgorithm

import (
	"fmt"
	"github.com/line/line-bot-sdk-go/linebot"
	"math"
	"nptuhackthon2019/database"
	"nptuhackthon2019/database/migrate"
	"nptuhackthon2019/global"
	"nptuhackthon2019/struct"
	"time"
)

var Player player_s

type player_s struct {
	exValueTable []int
}

func (this *player_s) Init() {
	this.exValueTable = []int{
		-1, //lv.0
		9,			//1
		16,			//2
		25,			//3
		33,			//4
		41,			//5
		49,			//6
		58,			//7
		66,			//8
		74,			//9
		82,			//10
		90,			//11
		99,			//12
		107,		//13
		115,		//14
		123,		//15
		131,		//16
		140,		//17
		148,		//18
		156,		//19
		-1, 		//20
	}
}

func (this *player_s) InfoStr(info *migrate.PlayerInfo) string {
	return fmt.Sprintf(
		"等級：%d\n" +
			"血量：%d => %d\n" +
			"攻擊力：%d => %d\n" +
			"經驗值：%d/%d\n" +
			"血量加成：%d%s\n" +
			"攻擊力加成：%d%s\n" +
			"發票金額：%d\n" +
			"戰鬥金額：%d\n" +
			"經驗加倍：%t",
			info.Level,
			info.HpMax, this.FinalHp(info.HpMax, info.HpBonus),
			info.Attack, this.FinalAttack(info.Attack, info.AttackBonus),
			info.ExValue, this.NextExValue(info.Level),
			info.HpBonus, "%",
			info.AttackBonus, "%",
			info.Belongings_invoice,
			info.Belongings_fight,
			info.HasDouble)
}

func (this *player_s) InfoStr2(info *_struct.User) string {
	return fmt.Sprintf(
			"血量：%d\n" +
			"攻擊力：%d\n",
			info.Hp,
			info.Attack,
			)
}

func (this *player_s) ToolRecordStr(info *migrate.PlayerInfo) string {
	str := ""
	for _, val := range info.HistoryBuy {
		str += val.Name + " => [" + val.CreatedAt.Format("2006-01-02 15:04") + "]" + "\n"
	}
	return str
}

func (this *player_s) NextExValue (Lv int) int {
	return this.exValueTable[Lv]
}

func (this *player_s) FinalHp(hp int, hpBonus int) int {
	var fBonus float64 = 1.0 + float64(hpBonus) * 0.01
	result := float64(hp) * fBonus + 0.5
	return int(math.Floor(result))
}

func (this *player_s) FinalAttack(ack int, ackBonus int) int {
	var fBonus float64 = 1.0 + float64(ackBonus) * 0.01
	result := float64(ack) * fBonus + 0.5
	return int(math.Floor(result))
}

func (this *player_s) FightEnd(winner, defeater *_struct.User) {
	var winUser migrate.PlayerInfo
	var isUpLevelStr string = ""
	if err := database.Db.Where("line_id=?", winner.Uid).First(&winUser).Error; err != nil {
		fmt.Println("fail to get winuser => ", err)
		return
	}
	AddExValue := 1
	AddBelongings := 1
	if winUser.HasDouble {
		AddExValue *= 2
		winUser.HasDouble = false
	}
	winUser.ExValue += AddExValue
	winUser.Belongings_fight += AddBelongings
	if winUser.ExValue >= this.NextExValue(winUser.Level) {
		winUser.Level += 1
		winUser.ExValue = 0
		winUser.HpMax += 3
		winUser.Attack += 3
		isUpLevelStr = fmt.Sprintf("恭喜您升等了(Lv.%d => Lv.%d)", winUser.Level - 1, winUser.Level)
	}
	if err := database.Db.Save(&winUser).Error; err != nil {
		fmt.Println("fail to update winuser => ", err)
		return
	}
	if _, err := global.LineBotApi.LinkUserRichMenu(winner.Uid, global.RichMenu["mainMenu"]).Do(); err != nil {
		fmt.Println("link fail => ", err)
		return
	}
	if _, err := global.LineBotApi.LinkUserRichMenu(defeater.Uid, global.RichMenu["mainMenu"]).Do(); err != nil {
		fmt.Println("link fail => ", err)
		return
	}

	winStr := fmt.Sprintf("戰鬥結束\n您勝利了\n\n獲得：\n經驗值：+%d\n戰鬥金錢：+%d\n%s", AddExValue, AddBelongings, isUpLevelStr)
	defeatStr := fmt.Sprintf("戰鬥結束\n您敗北了\n\n獲得：\n經驗值：+0\n戰鬥金錢：+0\n")

	if _, err := global.LineBotApi.PushMessage(winner.Uid, linebot.NewTextMessage(winStr)).Do(); err != nil {
		fmt.Println("replay fail", err)
		return
	}
	if _, err := global.LineBotApi.PushMessage(defeater.Uid, linebot.NewTextMessage(defeatStr)).Do(); err != nil {
		fmt.Println("replay fail", err)
		return
	}

	winner.State = _struct.Normal
	defeater.State = _struct.Normal
}

func (this *player_s) FightFinish(room *_struct.Room) {
	user1 := room.U1
	user2 := room.U2
	if user1.State != _struct.Fight_ok || user2.State != _struct.Fight_ok {
		return
	}
	if user1.Skill == _struct.Scissor && user2.Skill == _struct.Paper || user1.Skill == _struct.Paper && user2.Skill == _struct.Scissor {
		if user1.Skill > user2.Skill {
			//u1:paper u2:scissor winner:u2
			user1.Hp -= user2.Attack
		} else {
			//u1:scissor u2:paper winner:u1
			user2.Hp -= user1.Attack
		}
	} else {
		if user1.Skill > user2.Skill {
			user2.Hp -= user1.Attack
		} else if user1.Skill == user2.Skill {
			if user1.Hp > user2.Hp {
				user1.Hp -= 5
			} else if user1.Hp < user2.Hp {
				user2.Hp -= 5
			}
		} else {
			user1.Hp -= user2.Attack
		}
	}

	if user1.Hp <= 0 {
		Player.FightEnd(user2, user1)
	} else if user2.Hp <= 0 {
		Player.FightEnd(user1, user2)
	} else {
		user1.State = _struct.Fight_no
		user1.LastAckTime = time.Now()
		user2.State = _struct.Fight_no
		user2.LastAckTime = time.Now()
		if _, err := global.LineBotApi.PushMessage(user1.Uid, linebot.NewTextMessage(fmt.Sprintf("對方出：%s\n我方出：%s\n對方Hp：%d\n我方Hp：%d",_struct.Skill2Str(user2.Skill),_struct.Skill2Str(user1.Skill), user2.Hp, user1.Hp) )).Do(); err != nil {
			fmt.Println("replay fail", err)
			return
		}
		if _, err := global.LineBotApi.PushMessage(user2.Uid, linebot.NewTextMessage(fmt.Sprintf("對方出：%s\n我方出：%s\n對方Hp：%d\n我方Hp：%d",_struct.Skill2Str(user1.Skill),_struct.Skill2Str(user2.Skill), user1.Hp, user2.Hp) )).Do(); err != nil {
			fmt.Println("replay fail", err)
			return
		}
	}
}