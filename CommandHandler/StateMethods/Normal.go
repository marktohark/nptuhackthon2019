package StateMethods

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/line/line-bot-sdk-go/linebot"
	"nptuhackthon2019/GameAlgorithm"
	"nptuhackthon2019/StoreObject"
	"nptuhackthon2019/database"
	"nptuhackthon2019/database/migrate"
	"nptuhackthon2019/global"
	"nptuhackthon2019/struct"
)

func Normal(cmd string, event *linebot.Event, bot *linebot.Client) {
	switch cmd {
	case "productList":
		productList(event, bot)
	case "profile":
		profile(event, bot)
	case "toolRecord":
		toolRecord(event, bot)
	case "fireRecord":
	case "queue":
		queue(event, bot)
	}
}

func productList(event *linebot.Event, bot *linebot.Client) {
	uid := event.Source.UserID
	global.UserPool[uid].State = _struct.Store
	if _, err := bot.LinkUserRichMenu(uid, global.RichMenu["productList"]).Do(); err != nil {
		fmt.Println("link fail => ", err)
		return
	}
	if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage(StoreObject.ProductDescPrint())).Do(); err != nil {
		fmt.Println("replay fail", err)
		return
	}
}

func profile(event *linebot.Event, bot *linebot.Client) {
	var UserModel migrate.PlayerInfo
	uid := event.Source.UserID
	if err := database.Db.Where("line_id=?", uid).Find(&UserModel).Error; err != nil {
		fmt.Println("fail to get profile. => ", err)
		return
	}
	infoStr := GameAlgorithm.Player.InfoStr(&UserModel)
	if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage(infoStr)).Do(); err != nil {
		fmt.Println("replay fail", err)
		return
	}
}

func toolRecord(event *linebot.Event, bot *linebot.Client) {
	var UserModel migrate.PlayerInfo
	uid := event.Source.UserID
	if err := database.Db.Where("line_id=?", uid).Preload("HistoryBuy",
		func(db *gorm.DB) *gorm.DB {
		return db.Order("`history_prods`.`created_at` DESC").Limit(10)
	}).Find(&UserModel).Error; err != nil {
		fmt.Println("fail to get profile. => ", err)
		return
	}
	toolRdStr := GameAlgorithm.Player.ToolRecordStr(&UserModel)
	if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage(toolRdStr)).Do(); err != nil {
		fmt.Println("replay fail", err)
		return
	}
}

func queue(event *linebot.Event, bot *linebot.Client) {
	var UserModel migrate.PlayerInfo
	uid := event.Source.UserID

	if err := database.Db.Where("line_id=?", uid).Find(&UserModel).Error; err != nil {
		fmt.Println("fail to get profile. => ", err)
		return
	}
	User := global.UserPool[uid]
	User.Attack = GameAlgorithm.Player.FinalAttack(UserModel.Attack, UserModel.AttackBonus)
	User.Hp = GameAlgorithm.Player.FinalHp(UserModel.HpMax, UserModel.HpBonus)
	User.State = _struct.Queue

	global.QueueChannel <- User
	if _, err := bot.LinkUserRichMenu(uid, global.RichMenu["cancelQueue"]).Do(); err != nil {
		fmt.Println("link fail => ", err)
		return
	}
	if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("列隊等待中...")).Do(); err != nil {
		fmt.Println("replay fail", err)
		return
	}
}