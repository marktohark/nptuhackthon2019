package StateMethods

import (
	"fmt"
	"github.com/line/line-bot-sdk-go/linebot"
	"nptuhackthon2019/global"
	"nptuhackthon2019/struct"
	"strings"
)

func Queue(cmd string, event *linebot.Event, bot *linebot.Client) {
	if strings.ToLower(cmd) != "break" {
		if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("無此指令\n列隊等待中...")).Do(); err != nil {
			fmt.Println("replay fail", err)
			return
		}
		return
	}
	//取消列隊
	uid := event.Source.UserID
	global.UserPool[uid].State = _struct.Normal
	if _, err := global.LineBotApi.LinkUserRichMenu(uid, global.RichMenu["mainMenu"]).Do(); err != nil {
		fmt.Println("link fail => ", err)
		return
	}
	if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("取消列隊等待")).Do(); err != nil {
		fmt.Println("replay fail", err)
		return
	}
}