package StateMethods

import (
	"fmt"
	"github.com/line/line-bot-sdk-go/linebot"
	"nptuhackthon2019/GameAlgorithm"
	"nptuhackthon2019/global"
	"nptuhackthon2019/struct"
	"time"
)

func Fight_no(cmd string, event *linebot.Event, bot *linebot.Client) {
	sUser := global.UserPool[event.Source.UserID]
	var dUser *_struct.User
	if sUser.Room.U1.Uid != sUser.Uid {
		dUser = sUser.Room.U1
	} else {
		dUser = sUser.Room.U2
	}
	if cmd[0] == '#' {
		if _, err := bot.PushMessage(dUser.Uid, linebot.NewTextMessage(cmd)).Do(); err != nil {
			fmt.Println("replay fail", err)
			return
		}
		return
	}
	switch cmd {
	case "scissor":
		sUser.Skill = _struct.Scissor
		sUser.State = _struct.Fight_ok
		GameAlgorithm.Player.FightFinish(sUser.Room)
	case "stone":
		sUser.Skill = _struct.Stone
		sUser.State = _struct.Fight_ok
		GameAlgorithm.Player.FightFinish(sUser.Room)
	case "paper":
		sUser.Skill = _struct.Paper
		sUser.State = _struct.Fight_ok
		GameAlgorithm.Player.FightFinish(sUser.Room)
	case "defeat":
		GameAlgorithm.Player.FightEnd(dUser, sUser)
	case "report":
		timeRe := time.Now().Unix() - dUser.LastAckTime.Unix()
		if dUser.State == _struct.Fight_no && timeRe >= 300 {
			GameAlgorithm.Player.FightEnd(sUser, dUser)
		} else {
			if _, err := bot.PushMessage(sUser.Uid, linebot.NewTextMessage(fmt.Sprintf("檢舉失敗，對方還有：%d秒可用", 300 - timeRe))).Do(); err != nil {
				fmt.Println("replay fail", err)
				return
			}
		}
	default:
		if _, err := bot.PushMessage(sUser.Uid, linebot.NewTextMessage("如果要與對方對話，請加# => #你的對話文字對話文字")).Do(); err != nil {
			fmt.Println("replay fail", err)
			return
		}
	}
}