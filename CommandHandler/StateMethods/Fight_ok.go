package StateMethods

import (
	"fmt"
	"github.com/line/line-bot-sdk-go/linebot"
	"nptuhackthon2019/global"
	"nptuhackthon2019/struct"
)

func Fight_ok(cmd string, event *linebot.Event, bot *linebot.Client) {
	sUser := global.UserPool[event.Source.UserID]
	var dUser *_struct.User
	if sUser.Room.U1.Uid != sUser.Uid {
		dUser = sUser.Room.U1
	} else {
		dUser = sUser.Room.U2
	}
	if cmd[0] == '#' {
		if _, err := bot.PushMessage(dUser.Uid, linebot.NewTextMessage(cmd)).Do(); err != nil {
			fmt.Println("replay fail", err)
			return
		}
		return
	}
	if _, err := bot.PushMessage(sUser.Uid, linebot.NewTextMessage("如果要與對方對話，請加# => #你的對話文字對話文字")).Do(); err != nil {
		fmt.Println("replay fail", err)
		return
	}
}