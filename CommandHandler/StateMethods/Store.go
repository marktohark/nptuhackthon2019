package StateMethods

import (
	"fmt"
	"github.com/line/line-bot-sdk-go/linebot"
	"nptuhackthon2019/StoreObject"
	"nptuhackthon2019/global"
	"nptuhackthon2019/struct"
	"strconv"
	"strings"
)

func Store(cmd string, event *linebot.Event, bot *linebot.Client) {
	uid := event.Source.UserID
	productNum, err := strconv.Atoi(cmd)
	if err != nil {
		if strings.ToLower(cmd) == "break" {
			global.UserPool[uid].State = _struct.Normal
			if _, err := bot.LinkUserRichMenu(uid, global.RichMenu["mainMenu"]).Do(); err != nil {
				fmt.Println("link fail => ", err)
				return
			}
		} else {
			if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("沒有此指令")).Do(); err != nil {
				fmt.Println("replay fail", err)
				return
			}
		}
		return
	}
	if productNum < 1 || productNum > len(StoreObject.ProductList) {
		if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("沒有此指令，1 <= x <= " + strconv.Itoa(len(StoreObject.ProductList)))).Do(); err != nil {
			fmt.Println("replay fail", err)
			return
		}
		return
	}
	errStr := StoreObject.ProductList[productNum].Calc(uid)
	if errStr != nil {
		if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage(*errStr)).Do(); err != nil {
			fmt.Println("replay fail", err)
			return
		}
	} else {
		if _, err := bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("未知錯誤。")).Do(); err != nil {
			fmt.Println("replay fail", err)
			return
		}
	}
}