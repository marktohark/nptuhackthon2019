package CommandHandler

import (
	"fmt"
	"github.com/line/line-bot-sdk-go/linebot"
	"nptuhackthon2019/CommandHandler/StateMethods"
	"nptuhackthon2019/global"
	"nptuhackthon2019/struct"
)

type cmdHandlerFunc func(string, *linebot.Event,*linebot.Client)

var cmdList map[_struct.State]cmdHandlerFunc = map[_struct.State]cmdHandlerFunc {
	_struct.Normal: StateMethods.Normal,
	_struct.Queue: StateMethods.Queue,
	_struct.Fight_no: StateMethods.Fight_no,
	_struct.Fight_ok: StateMethods.Fight_ok,
	_struct.Store: StateMethods.Store,
}

func Parser(cmd string, event *linebot.Event, bot *linebot.Client) {
	uid := event.Source.UserID
	user, ok := global.UserPool[uid]
	if (!ok) {
		fmt.Println("user is not exist... =>", uid)
		return
	}
	_func, _ := cmdList[user.State]
	_func(cmd, event, bot)
}