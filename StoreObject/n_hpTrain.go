package StoreObject

import "nptuhackthon2019/database/migrate"

func n_hpTrain(uid string) *string {
	return buyProduct(uid, 4, func(UserModel *migrate.PlayerInfo, prodNum int) {
		UserModel.HpBonus += 1
		UserModel.Belongings_fight -= ProductList[prodNum].Price
	})
}

