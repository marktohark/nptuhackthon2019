package StoreObject

import "nptuhackthon2019/database/migrate"

func n_attackTrain(uid string) *string {
	return buyProduct(uid, 5, func(UserModel *migrate.PlayerInfo, prodNum int) {
		UserModel.AttackBonus += 1
		UserModel.Belongings_fight -= ProductList[prodNum].Price
	})
}
