package StoreObject

import (
	"nptuhackthon2019/database/migrate"
)

func attackTrain(uid string) *string {
	return buyProduct(uid, 2, func(UserModel *migrate.PlayerInfo, prodNum int) {
		UserModel.AttackBonus += 10
		UserModel.Belongings_invoice -= ProductList[prodNum].Price
	})
}

