package StoreObject

import (
	"nptuhackthon2019/database/migrate"
)

func hpTrain(uid string) *string {
	return buyProduct(uid, 1, func(UserModel *migrate.PlayerInfo, prodNum int) {
		UserModel.HpBonus += 10
		UserModel.Belongings_invoice -= ProductList[prodNum].Price
	})
}