package StoreObject

import (
	"nptuhackthon2019/database/migrate"
)

func doubleEx(uid string) *string {
	return buyProduct(uid, 3, func(UserModel *migrate.PlayerInfo, prodNum int) {
		UserModel.HasDouble = true
		UserModel.Belongings_invoice -= ProductList[prodNum].Price
	})
}