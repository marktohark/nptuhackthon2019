package StoreObject

import (
	"fmt"
	"nptuhackthon2019/database"
	"nptuhackthon2019/database/migrate"
	"strconv"
)

type ProdCalcFunc func(string)*string

type Product struct {
	Name string
	Price int
	Desc string
	Type bool //false: 發票  true: 對戰
	Calc ProdCalcFunc
}

var ProductList map[int]Product

func Init() {
	ProductList = map[int]Product{
		1: Product{
			Name: "血量鍛鍊器",
			Price: 1,
			Desc: "1. 血量鍛鍊器 => [永久][加血量][10%]",
			Type: false,
			Calc: hpTrain,
		},
		2: Product{
			Name: "攻擊力鍛鍊器",
			Price: 1,
			Desc: "2. 攻擊力鍛鍊器 => [永久][加攻擊力][10%]",
			Type: false,
			Calc: attackTrain,
		},
		3: Product{
			Name: "雙倍經驗券",
			Price: 1,
			Desc: "3. 雙倍經驗券 => [加倍][下一場經驗值加倍][不可重複使用]",
			Type: false,
			Calc: doubleEx,
		},
		4: Product{
			Name: "劣質血量鍛鍊器",
			Price: 1,
			Desc: "4. 劣質血量鍛鍊器 => [永久][加血量][1%]",
			Type: true,
			Calc: n_hpTrain,
		},
		5: Product{
			Name: "劣質攻擊力鍛鍊器",
			Price: 1,
			Desc: "5. 劣質攻擊力鍛鍊器 => [永久][加攻擊力][1%]",
			Type: true,
			Calc: n_attackTrain,
		},
	}
}

func ProductDescPrint() string {
	str := ""
	for i := 1; i <= 6; i++ {
		str += ProductList[i].Desc + "\n"
	}
	return str
}

func makeBuyFail(curPrice int, needPrice int, _type bool) string {
	priceName := ""
	if _type {
		priceName = "戰鬥"
	} else {
		priceName = "發票"
	}
	return "購買失敗\n當前" + priceName + "金額：" + strconv.Itoa(curPrice) + "\n所需" + priceName + "金額：" + strconv.Itoa(needPrice)
}

func makeBuySuccess(curPrice int, _type bool) string {
	priceName := ""
	if _type {
		priceName = "戰鬥"
	} else {
		priceName = "發票"
	}
	return "購買成功\n" + priceName + "金額剩餘：" + strconv.Itoa(curPrice)
}

func buyProduct(uid string, prodNum int, UserInfoHandler func(*migrate.PlayerInfo, int)) *string {
	var UserModel migrate.PlayerInfo
	tx := database.Db.Begin()
	if err := tx.Where("line_id = ?", uid).First(&UserModel).Error; err != nil {
		tx.Rollback()
		fmt.Println("hpTrain => query fail => ", err)
		return nil
	}
	if !ProductList[prodNum].Type {
		if UserModel.Belongings_invoice < ProductList[prodNum].Price {
			tx.Rollback()
			t := makeBuyFail(UserModel.Belongings_invoice, ProductList[prodNum].Price, ProductList[prodNum].Type)
			return &t
		}
	} else {
		if UserModel.Belongings_fight < ProductList[prodNum].Price {
			tx.Rollback()
			t := makeBuyFail(UserModel.Belongings_fight, ProductList[prodNum].Price, ProductList[prodNum].Type)
			return &t
		}
	}
	//
	UserInfoHandler(&UserModel, prodNum)
	//
	if err := tx.Save(&UserModel).Error; err != nil {
		tx.Rollback()
		fmt.Println("hpTrain => save fail => ", err)
		return nil
	}
	if err := tx.Model(&UserModel).Association("HistoryBuy").Append(migrate.HistoryProd{
		Name: ProductList[prodNum].Name,
	}).Error; err != nil {
		tx.Rollback()
		fmt.Println("hpTrain => relationship insert fail => ", err)
		return nil
	}
	tx.Commit()
	t := ""
	if !ProductList[prodNum].Type {
		t = makeBuySuccess(UserModel.Belongings_invoice, ProductList[prodNum].Type)
	} else {
		t = makeBuySuccess(UserModel.Belongings_fight, ProductList[prodNum].Type)
	}

	return &t
}