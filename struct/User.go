package _struct

import "time"

type State int
const (
	State_Begin State = iota
	Normal		//大廳
	Queue		//列隊等待中
	Fight_no	//尚未出技能
	Fight_ok	//已經出技能
	Store		//購物中
	State_End
)

type Skill int
const (
	Skill_Begin Skill = iota
	Scissor		//剪刀
	Stone		//石頭
	Paper		//布
	Skill_End
)

func Skill2Str(sk Skill) string {
	if sk == Scissor {
		return "剪刀"
	}
	if sk == Stone {
		return "石頭"
	}
	if sk == Paper {
		return "布"
	}
	return "未知"
}

type User struct {
	Uid string
	State State
	Hp int
	Attack int
	LastAckTime time.Time
	Skill Skill
	Room *Room
}