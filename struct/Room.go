package _struct

import "time"

type Room struct {
	U1 *User
	U2 *User
	Winner string
	CreateDate time.Time
}